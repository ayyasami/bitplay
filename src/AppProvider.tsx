import React from "react";
import { BrowserRouter } from "react-router-dom";
import { AppRoutes } from "routes";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "theme";
import { Appshell } from "./Appshell";

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <Appshell>
          <AppRoutes />
        </Appshell>
        <GlobalStyle />
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
