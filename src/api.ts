import axios from "axios";

export const fetchAlbum = async ({ type }: { type: "series" | "movie" }) => {
  return axios
    .get(
      "https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json"
    )
    .then(({ data }: any) => {
      return data.entries
        .filter(
          (entry: any) =>
            entry.releaseYear >= 2010 && entry.programType === type
        )
        .slice(0, 21);
    });
};
