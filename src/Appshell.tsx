import { Footer } from "component/Footer";
import { Header } from "component/Header";
import React from "react";
import styled from "styled-components";
export const ContentWrapper = styled("div")`
  padding: 0 calc(50vw - 550px);
  margin: 0 0.5rem;
`;
interface AppShellProps {}
export const Appshell: React.FC<AppShellProps> = (props) => {
  return (
    <React.Fragment>
      <Header />
      <ContentWrapper>{props.children}</ContentWrapper>
      <Footer />
    </React.Fragment>
  );
};
