import { StyledLink } from "component/StyledLink";
import * as React from "react";
import styled from "styled-components";
import placeholder from "../assets/placeholder.png";

const HomePageDiv = styled("div")`
  display: flex;
  column-gap: 1rem;
  margin: 1rem 0;
  min-height: 30rem;
`;
const PlaceholderDiv = styled("div")`
  width: 9rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: ${(props) => props.theme.colors.secondary};
`;

const PlaceholderImgDiv = styled("div")<{ backgroundImg: any }>`
  height: 11rem;
  width: 19rem;
  background-size: cover;
  display: flex;
  align-items: center;
  background-image: url(${(props) => props.backgroundImg});
  > span {
    color: ${(props) => props.theme.colors.white};
    font-size: x-large;
    margin: auto;
  }
`;

export const HomePage = () => {
  return (
    <HomePageDiv>
      <AlbumCategoryCard to="/series" caption="Series" title="Popular Series" />
      <AlbumCategoryCard to="/movies" caption="Movies" title="Popular Movies" />
    </HomePageDiv>
  );
};
interface AlbumCategoryCardProps {
  to: string;
  caption: string;
  title: string;
}
const AlbumCategoryCard: React.FC<AlbumCategoryCardProps> = ({
  to,
  title,
  caption,
}) => (
  <StyledLink to={to}>
    <PlaceholderDiv>
      <PlaceholderImgDiv backgroundImg={placeholder}>
        <span>{caption}</span>
      </PlaceholderImgDiv>
    </PlaceholderDiv>
    <span>{title}</span>
  </StyledLink>
);
