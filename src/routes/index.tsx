import { Route, Switch } from "react-router";
import { HomePage } from "./HomePage";
import { MoviesPage } from "./MoviesPage";
import { SeriesPage } from "./SeriesPage";

export const AppRoutes: React.FC = () => {
  return (
    <Switch>
      <Route exact={true} path="/" render={() => <HomePage />} />
      <Route path="/series" component={SeriesPage} />
      <Route path="/movies" component={MoviesPage} />
    </Switch>
  );
};
