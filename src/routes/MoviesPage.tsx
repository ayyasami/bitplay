import * as React from "react";
import { fetchAlbum } from "../api";
import { AlbumCard, AlbumContainer } from "./SeriesPage";

export const MoviesPage = () => {
  const [context, setContext] = React.useState<{
    loading: boolean;
    series: any[];
    error?: any;
  }>({ loading: true, error: null, series: [] });

  React.useEffect(() => {
    fetchAlbum({ type: "movie" })
      .then((data: any) => {
        setContext({ loading: false, series: data });
      })
      .catch((er: any) => {
        console.log("ERROR", er);
        setContext({ loading: false, error: er, series: [] });
      });
  }, []);
  if (context.loading) {
    return <div>Loading...</div>;
  }
  if (context.error) {
    return <div>Oops Something went wrong...</div>;
  }
  return (
    <AlbumContainer>
      {context.series.map(
        (
          {
            images: {
              "Poster Art": { url },
            },
            title,
          }: any,
          idx: number
        ) => (
          <AlbumCard key={idx}>
            <img src={url} alt={title} />
            <span>{title}</span>
          </AlbumCard>
        )
      )}
    </AlbumContainer>
  );
};
