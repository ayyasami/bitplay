import * as React from "react";
import styled from "styled-components";
import { fetchAlbum } from "../api";

export const AlbumContainer = styled("div")`
  display: grid;
  margin: 1rem 0;
  ${(props) => props.theme.media.mobile} {
    grid-template-columns: repeat(3, 1fr);
  }
  ${(props) => props.theme.media.tablet} {
    grid-template-columns: repeat(5, 1fr);
  }
  ${(props) => props.theme.media.desktop} {
    grid-template-columns: repeat(7, 1fr);
  }
`;
export const AlbumCard = styled("div")`
  max-width: 143px;
  min-width: 100px;
  img {
    max-width: inherit;
    min-width: inherit;
  }
`;
export const SeriesPage = () => {
  const [context, setContext] = React.useState<{
    loading: boolean;
    series: any[];
    error?: any;
  }>({ loading: true, error: null, series: [] });

  React.useEffect(() => {
    fetchAlbum({ type: "series" })
      .then((data: any) => {
        setContext({ loading: false, series: data });
      })
      .catch((er: any) => {
        console.log("ERROR", er);
        setContext({ loading: false, error: er, series: [] });
      });
  }, []);
  if (context.loading) {
    return <div>Loading...</div>;
  }
  if (context.error) {
    return <div>Oops Something went wrong...</div>;
  }
  return (
    <AlbumContainer>
      {context.series.map(
        (
          {
            images: {
              "Poster Art": { url },
            },
            title,
          }: any,
          idx: number
        ) => (
          <AlbumCard key={idx}>
            <img src={url} alt={title} />
            <span>{title}</span>
          </AlbumCard>
        )
      )}
    </AlbumContainer>
  );
};
