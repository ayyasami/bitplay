import { createGlobalStyle, DefaultTheme } from "styled-components";

export const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,700&display=swap');
html{
  font-size: 16px;
  ${(props) => props.theme.media.desktop}{
    font-size:18px;
  } 
}
body {
  margin: 0 !important;
}
body * {
  font-family: ${(props) => props.theme.fontFamily};
}
`;
export const theme: DefaultTheme = {
  fontFamily: "Raleway, sans-serif",
  media: {
    mobile: "@media (max-width: 599px)",
    tablet: "@media (min-width: 600px)",
    desktop: "@media (min-width: 992px)",
  },
  colors: {
    primary: "#1c88fb",
    secondary: "#414141",
    white: "#ffffff",
    black: "#000000",
  },
};
