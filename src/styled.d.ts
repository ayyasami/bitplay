import "styled-components";

interface IMedia {
  mobile: string;
  tablet: string;
  desktop: string;
}
interface IColor {
  primary: string;
  secondary: string;
  white: string;
  black: string;
}
interface ITheme {
  fontFamily: string;
  media: IMedia;
  colors: IColor;
}
export declare module "styled-components" {
  export interface DefaultTheme extends ITheme {}
}
