import * as React from "react";
import { theme } from "theme";

interface IconProps {
  name: string;
  url?: string;
  style?: any;
}

export const ExternalIcon: React.FC<IconProps> = ({ ...props }) => {
  return <Icon {...props} />;
};
ExternalIcon.defaultProps = {
  url: "/assets/externalIcon.svg",
};
const Icon: React.FC<IconProps> = ({ ...props }) => {
  return (
    <svg viewBox="0 0 16 16" {...props}>
      <use xlinkHref={`${props.url}#${props.name}`} />
    </svg>
  );
};
export const SiteIcon: React.FC<IconProps> = ({ ...props }) => {
  return (
    <Icon
      {...props}
      style={{
        width: "1rem",
        fill: theme.colors.primary,
        margin: ".5rem",
        ...props.style,
      }}
    />
  );
};
Icon.defaultProps = {
  url: "/assets/icon.svg",
};
