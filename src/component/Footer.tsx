import { ContentWrapper } from "Appshell";
import * as React from "react";
import styled from "styled-components";
import { theme } from "theme";
import { ReactComponent as AppStore } from "../assets/store/app-store.svg";
import { ReactComponent as PlayStore } from "../assets/store/play-store.svg";
import { ReactComponent as WindowsStore } from "../assets/store/windows-store.svg";
import { SiteIcon } from "./Icon";
import { StyledLink } from "./StyledLink";

const FooterContainer = styled("div")`
  padding: 1rem 0;
  background: ${(props) => props.theme.colors.secondary};
  color: ${(props) => props.theme.colors.white};
  font-size: small;
  font-weight: 200;
`;
const navLinks: { caption: string; to: string }[] = [
  {
    caption: "Home",
    to: "/",
  },
  {
    caption: "Terms and Condition",
    to: "/terms",
  },
  {
    caption: "Provacy and Policy",
    to: "/policy",
  },
  {
    caption: "Help",
    to: "/help",
  },
];
const socialLinks = [
  { name: "facebook", link: "/" },
  { name: "instagram", link: "/" },
  { name: "twitter", link: "/" },
];
const LinkDiv = styled("div")`
  display: flex;
  justify-content: space-between;
  gap: 1rem;
  ${props=> props.theme.media.mobile}{
    flex-direction: column;
  }
`;
const SocialDiv = styled("div")`
  display: flex;
  gap: 0.5rem;
`;
const StoreDiv = styled("div")`
  display: flex;
  gap: 1rem;
  ${props=> props.theme.media.mobile}{
    flex-direction: column;
    gap: 0.5rem;
  }
`;
export const Footer: React.FC = () => {
  return (
    <footer>
      <FooterContainer>
        <ContentWrapper>
          <p>
            {navLinks.map(({ caption, to }: any, idx: number) => (
              <React.Fragment key={idx}>
                {!!idx && <span> | </span>}
                <span key={idx}>
                  <StyledLink to={to}>{caption}</StyledLink>
                </span>
              </React.Fragment>
            ))}
          </p>
          <p>Copyrights 2020 BitPlay Streaming. All rights reserved</p>
          <LinkDiv>
            <SocialDiv>
              {socialLinks.map((socialLink: any, idx: number) => (
                <StyledLink key={idx} to={socialLink.link}>
                  <SiteIcon
                    name={socialLink.name}
                    style={{ fill: theme.colors.white, margin: "0" }}
                  />
                </StyledLink>
              ))}
            </SocialDiv>
            <StoreDiv>
              <AppStore />
              <PlayStore />
              <WindowsStore />
            </StoreDiv>
          </LinkDiv>
        </ContentWrapper>
      </FooterContainer>
    </footer>
  );
};
