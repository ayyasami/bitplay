import React from "react";
import styled from "styled-components";

interface ButtonProps {
  color: "primary" | "secondary" | "invisible";
  children: any;
}
const ButtonComp = styled("button")<ButtonProps>`
  padding: 0.5rem;
  font-size: 1rem;
  cursor: pointer;
  color: white;
  font-weight: 400;
  background: ${(props) =>
    props.color === "secondary"
      ? props.theme.colors.secondary
      : props.color === "invisible"
      ? "inherit"
      : props.theme.colors.secondary};
  border: ${(props) =>
    props.color === "primary" ? "solid 1px white" : "none"};
`;

export const Button: React.FC<ButtonProps> = (props) => {
  return <ButtonComp color={props.color}>{props.children}</ButtonComp>;
};
