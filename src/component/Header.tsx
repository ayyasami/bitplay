import { ContentWrapper } from "Appshell";
import React, { FC } from "react";
import styled from "styled-components";
import { Button } from "./Button";

const HeaderComp = styled("header")`
  background-image: linear-gradient(to right, #057cfbeb, #057cfbe6);
`;
const HeaderContainer = styled("div")`
  padding: 0.5rem 0;
  display: flex;
  height: 4rem;
  flex-flow: row;
  justify-content: space-between;
  align-items: center;
  button:nth-child(2){
    ${props=>props.theme.media.mobile}{
      span {
        display: none;
      }
    }
  }
`;
const Logo = styled("a")`
  text-decoration: none;
  color: white;
  font-size: xx-large;
  font-weight: 500;
`;

export const Header: FC = () => {
  return (
    <HeaderComp>
      <ContentWrapper>
        <HeaderContainer>
          <Logo href="/">BitPlay Streaming</Logo>
          <div>
            <Button color={"invisible"}>Login</Button>
            <Button color="secondary"><span>Start your free</span> trial</Button>
          </div>
        </HeaderContainer>
      </ContentWrapper>
    </HeaderComp>
  );
};
